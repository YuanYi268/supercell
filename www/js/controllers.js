angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})
.controller('LoginCtrl', function($scope) {

  /*  $scope.leftButtons = [{
    type: 'button-icon icon ion-navicon',
    tap: function(e) {
      console.log('Going back!');
    }
  }];*/


})

.controller('InfosCtrl', function($scope, Infos) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.infos = Infos.all();
  $scope.remove = function(info) {
    Infos.remove(info);
  };
})

.controller('InfoDetailCtrl', function($scope, $stateParams, Infos) {
  $scope.info = Infos.get($stateParams.infoId);
})

.controller('RemindMedCtrl', function($scope,$location){
  $scope.go = function (remindMed) {
  $location.path('tab.remindMed.html');
}
})
  

.controller('HelpCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
