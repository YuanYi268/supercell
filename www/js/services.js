angular.module('starter.services', [])

.factory('Infos', function() {
  // Might use a resource here that returns a JSON array

  // testing data
  var infos = [{
    id: 0,
    path : 'medical',
    title: 'Mecidal ID',
    views :'tab-login',
    icon : 'ios-medical-outline'
    // $state.go:'tab-dash'
  
  }, {
    id: 1,
    path : 'personal',
    title: 'My Personal Info',
    icon : 'ios-person-outline'
   
  }, {
    id: 2,
    path : 'setting',
    title: 'Settings',
    icon : 'ios-gear-outline'
   
  }, {
    id: 3,
    path : 'qr-scan',
    title: 'QR Scan',
    icon : 'qr-scanner'
   
  }, {
    id: 4,
    path : 'help-feedback',
    title: 'Help & Feedback',
    icon : 'ios-chatbubble-outline'
   
  },{
    id: 5,
    path : 'share',
    title: 'Share',
    icon : 'ios-upload-outline'
  }
  ];

  return {
    all: function() {
      return infos;
    },
    remove: function(info) {
      infos.splice(infos.indexOf(info), 1);
    },
    get: function(infoId) {
      for (var i = 0; i < infos.length; i++) {
        if (infos[i].id=== parseInt(infoId)) {
          return infos[i];
        }
      }
      return null;
    }
  };
});
